import re, pdb
import numpy as np

re_epoch = re.compile("Epoch (\\d+): (\\d+) ms")
re_loss = re.compile("Loss = (\\d+\\.\\d+)")
re_prf = re.compile("P = (0\\.\\d+); R = (0\\.\\d+); F1 = (0\\.\\d+)")

def digest(filename):
  f = open(filename)
  line = f.readline()
  time = []
  loss = []
  prf = []
  while line != "":
    # epoch 
    epoch_match = re_epoch.match(line)
    if not epoch_match:
      pdb.set_trace()
    time.append(float(epoch_match.group(2)))

    line = f.readline()

    # loss
    loss_match = re_loss.match(line)
    if not loss_match:
      loss.append(np.nan)
    else:
      loss.append(float(loss_match.group(1)))

    line = f.readline()

    # prf
    prf_match = re_prf.match(line)
    if not prf_match:
      prf.append(np.nan)
    else:
      prf.append((float(prf_match.group(1)), float(prf_match.group(2)), float(prf_match.group(3))))
    
    line = f.readline()
  return time, loss, prf


import plotly.plotly as py
from plotly.graph_objs import *
import digest, sys

if __name__ == "__main__":
  traces = []
  for filename in ["1-false", "16-false", "2-false", "32-false", "4-false", "8-false"]:
  # for filename in ["1-true", "16-true", "2-true", "32-true", "4-true", "8-true"]:
    _, loss, _ = digest.digest("speedup/" + filename + ".log")
    traces.append(Scatter(
      x=range(1, len(loss) + 1),
      y=loss,
      name=filename,
    ))
    data = Data(traces)
  layout = Layout(
      xaxis=XAxis(
          title='epoch'
      ),
      yaxis=YAxis(
          title='loss'
      )
  )
  fig = Figure(data=data, layout=layout)
  plot_url = py.plot(fig)


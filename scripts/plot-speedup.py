import plotly.plotly as py
from plotly.graph_objs import *
import digest, sys, pdb

if __name__ == "__main__":
  averaged_times = []
  traces = []
  for policy in ["locked", "hogwild"]:
    for threadn in [1, 2, 4, 8, 16, 32]:
      if policy == "locked":
        time, _, _ = digest.digest(str(threadn) + "-true.log")
      else:
        time, _, _ = digest.digest(str(threadn) + "-false.log")
      averaged_times.append(sum(time) / 20.0)
  
    speedup = []
    for i in range(0, len(averaged_times)):
      speedup.append(averaged_times[0] / averaged_times[i])

    traces.append(Scatter(
      x=[1, 2, 4, 8, 16, 32],
      y=speedup,
      name=policy,
    ))

  data = Data(traces)
  layout = Layout(
      xaxis=XAxis(
          title='number of threads'
      ),
      yaxis=YAxis(
          title='speedup factor'
      )
  )
  fig = Figure(data=data, layout=layout)
  plot_url = py.plot(fig)


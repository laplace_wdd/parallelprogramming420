import plotly.plotly as py
from plotly.graph_objs import *
import digest, sys

if __name__ == "__main__":
  all_fpr = []
  for filename in ["1-false", "2-false", "4-false", "8-false", "16-false", "32-false", "1-true", "2-true", "32-true", "4-true", "8-true", "16-true", "32-true"]:
  # for filename in ["1-true", "16-true", "2-true", "32-true", "4-true", "8-true"]:
    _, _, prf = digest.digest("speedup/" + filename + ".log")
    fpr = [(f, p, r) for (p, r, f) in prf]
    all_fpr.append(max(fpr))

  for fpr in all_fpr:
    (f, p, r) = fpr
    print "{0},\t{1},\t{2}".format(p, r, f)

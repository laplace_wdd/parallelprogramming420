cd $HOME/final/proj-scala
BATCH_SIZE=80
EPOCH_SIZE=20
LEARNING_RATE=0.1

mkdir -p $HOME/logs/scaleup
for threadn in 1 2 4 8 16 32; do
  for lock in "true" "false"; do
    java -cp target/scala-2.11/*.jar par.Main ../data/gen_spam/train/gen_spam-times$(($threadn * 8)) ../data/gen_spam/test/gen_spam $threadn $((BATCH_SIZE * $threadn)) $EPOCH_SIZE $LEARNING_RATE $lock > $HOME/logs/scaleup/$threadn-$lock.log 2>&1
  done
done


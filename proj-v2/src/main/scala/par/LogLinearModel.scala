package par

import scala.collection.mutable.HashMap

/**
 * @author Tongfei Chen
 */
class LogLinearModel(val w: Array[Double]) {

  import LogLinearModel._

  def loss(data: Seq[(Int, HashMap[Int, Double])]) = {
    var sum = 0.0
    for ((y, x) <- data) {
      val product = w dot x
      val expProduct = math.exp(-product)
      val score = if (y == 0) 1.0 / (1.0 + expProduct) else expProduct / (1.0 + expProduct)
      val logLoss = math.log(score)
      sum -= logLoss
    }
    sum
  }

  def predict(x: HashMap[Int, Double]) = {
    if ((w dot x) <= 0) 1 else 0
  }

  def lockedUpdate(y: SpVec) = w.synchronized {
    for ((i, v) <- y)
      w(i) -= v
  }

  def hogwildUpdate(y: SpVec) = {
    for ((i, v) <- y)
      w(i) -= v
  }

}


object LogLinearModel {

  type Vec = Array[Double]
  type SpVec = HashMap[Int, Double]

  implicit class VecOps(val x: Vec) extends AnyVal {

    def -=(y: SpVec): Unit = {
      for ((i, v) <- y)
        x(i) -= v
    }

    def +(y: Vec) = {
      val z = new Array[Double](x.length)
      for (i <- 0 until x.length) z(i) = x(i) + y(i)
      z
    }

    def *(k: Double) = {
      val z = new Array[Double](x.length)
      for (i <- 0 until x.length) z(i) = x(i) * k
      z
    }

    def dot(y: SpVec) = {
      var z = 0.0
      for ((i, v) <- y) z += v * x(i)
      z
    }

  }

  implicit class SpVecOps(val x: SpVec) extends AnyVal {
    def *(k: Double) = x map { case (i, v) => (i, k * v) }

    def +(y: SpVec) = {
      val z = new HashMap[Int, Double]
      for ((i, v) <- x) {
        if (z contains i) z(i) += v
        else z += i -> v
      }
      for ((i, v) <- y) {
        if (z contains i) z(i) += v
        else z += i -> v
      }
      z
    }

    def addInplace(y: SpVec) = {
      for ((i, v) <- y) {
        if (x contains i) x(i) += v
        else x += i -> v
      }
    }
  }

  def time[A](op: => A): Long = {
    val t0 = System.currentTimeMillis()
    op
    val t1 = System.currentTimeMillis()
    t1 - t0
  }

  def fit(
           data: Array[(Int, HashMap[Int, Double])],
           testData: Array[(Int, HashMap[Int, Double])],
           numThreads: Int,
           batchSize: Int,
           numEpochs: Int,
           learningRate: Double,
           locked: Boolean
         ) = {

    val model = new LogLinearModel(new Array[Double](DataReader.alphabet.size))

    val numBatchSamples = batchSize / numThreads

    for (epoch <- 0 until numEpochs) {

      val epochDuration = time {
        for (batchStart <- 0 until data.length by batchSize) {

          val batchEnd = (batchStart + batchSize) min data.length

          val threads = Array.ofDim[Thread](numThreads)

          for (threadId <- (0 until (numThreads min ((batchEnd - batchStart) / numBatchSamples)))) {
            threads(threadId) = new Thread(new SgdThread(
              data,
              batchStart + threadId * numBatchSamples,
              (batchStart + (threadId + 1) * numBatchSamples) min batchEnd,
              model,
              learningRate,
              locked
            ))
            threads(threadId).start()
          }

          for (i <- 0 until numThreads)
            if (threads(i) != null)
              threads(i).join()
        }
      }

      System.err.println(s"Epoch $epoch: $epochDuration ms")

      System.err.println(s"Loss = ${model.loss(data)}")

      var tp, fp, tn, fn = 0.0
      for ((y, x) <- testData) {
        (y, model predict x) match {
          case (1, 1) => tp += 1
          case (1, 0) => fn += 1
          case (0, 1) => fp += 1
          case (0, 0) => tn += 1
        }
      }
      val precision = tp / (tp + fp)
      val recall = tp / (tp + fn)
      val f1 = 2 * precision * recall / (precision + recall)

      System.err.println(s"P = $precision; R = $recall; F1 = $f1")

    }

    model
  }

}

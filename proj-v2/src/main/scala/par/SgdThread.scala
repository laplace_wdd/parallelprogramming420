package par

import scala.collection.mutable.HashMap

/**
 * @author Tongfei Chen
 */
class SgdThread(data: Array[(Int, HashMap[Int, Double])], begin: Int, end: Int, model: LogLinearModel, learningRate: Double, locked: Boolean) extends Runnable {

  import LogLinearModel._

  def gradient(y: Int, x: HashMap[Int, Double]) = {
    val product = math.exp(-(model.w dot x))
    val score = product / (1 + product)
    x * (y - score)
  }

  def run() = {

    val totalGradient = HashMap[Int, Double]()
    for (i <- begin until end) {
      val (y, x) = data(i)
      totalGradient addInplace gradient(y, x)
    }
    if (locked) model.lockedUpdate(totalGradient * learningRate)
    else model.hogwildUpdate(totalGradient * learningRate)
  }

}

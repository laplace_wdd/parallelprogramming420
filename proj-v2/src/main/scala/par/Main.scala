package par

/**
 * @author Tongfei Chen
 */
object Main extends App {

  val data = DataReader.read(args(0))
  val testData = DataReader.read(args(1))
  val numThreads = args(2).toInt
  val batchSize = args(3).toInt
  val numEpochs = args(4).toInt
  val learningRate = args(5).toDouble
  val locked = if (args(6) == "true") true else false

  LogLinearModel.fit(data, testData, numThreads, batchSize, numEpochs, learningRate, locked)

}

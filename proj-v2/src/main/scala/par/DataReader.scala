package par

import scala.collection.mutable.HashMap
import scala.io._
/**
 * @author Tongfei Chen
 */
object DataReader {

  val alphabet = Alphabet("<OOV>")

  def read(filename: String) = {

    (for (line <- Source.fromFile(new java.io.File(filename)).getLines) yield {

      val tokens = line split "\\s+"
      val label = tokens.head.toInt
      val words = tokens.tail.map(_.toLowerCase)

      val unigramFeatures = words map { w => alphabet(w) }


      val bigramFeatures = if (words.size > 2) {
        words sliding 2 map { pair =>
          alphabet(pair(0) + "$" + pair(1))
        }
      } else Seq()

      val set = (unigramFeatures.view ++ bigramFeatures).map(_ -> 1.0)

      label -> HashMap(set: _*)
    }).toArray

  }

}

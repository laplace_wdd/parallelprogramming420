package par

import scala.collection.mutable._

/**
 * Represents a mutable alphabet.
 *
 * @since 0.1.0
 * @author Tongfei Chen
 */
class Alphabet private(w2i: HashMap[String, Int], i2w: ArrayBuffer[String]) {

  private[this] var frozen = false

  def apply(x: String): Int = synchronized {
    if (frozen) w2i.getOrElse(x, 0)
    else {
      w2i.get(x) match {
        case Some(i) => i
        case None =>
          val newIndex = w2i.size
          w2i += x -> newIndex
          i2w += x
          newIndex
      }
    }
  }

  def size = i2w.size

  def invert(i: Int) = i2w(i)

  def freeze() = synchronized {
    frozen = true
  }

  def unfreeze() = synchronized {
    frozen = false
  }

}

object Alphabet {

  def apply(nil: String) = new Alphabet(HashMap(nil -> 0), ArrayBuffer(nil))

}
package model;

/**
 * Created by dqwang on 12/8/16.
 */

import data.InstanceIterator;
import edu.jhu.pacaya.gm.feat.FeatureVector;
import edu.jhu.pacaya.gm.maxent.LogLinearXY.LogLinearXYPrm;
import edu.jhu.pacaya.gm.model.FgModel;
import edu.jhu.prim.bimap.IntObjectBimap;
import edu.jhu.prim.vector.IntDoubleVector;
import org.apache.log4j.Logger;
import util.Instance;

import java.io.IOException;
import java.util.List;

public class Model {

    private static final Logger log = Logger.getLogger(Model.class);

    private FgModel model = null;
    private LogLinearModel logLinearModel;
    private IntObjectBimap<String> alphabet;

    private final static int IGNORED_X = 0;

    public Model(LogLinearXYPrm prm) {
        if (prm == null) prm = new LogLinearXYPrm();
        this.logLinearModel = new LogLinearModel(prm);
    }

    public static class CallbackFunction {
        protected Model model;

        public void register(Model model) {
            this.model = model;
        }

        public void onTrainBegin() {
        }

        public void onTrainEnds() {
        }

        public void onEpochBegin() {
        }

        public void onEpochEnd() throws IOException {
        }

    }

    public static class EvalDataFunction extends CallbackFunction {
        private InstanceIterator instanceIterator;

        public EvalDataFunction(InstanceIterator instanceIterator) {
            super();
            this.instanceIterator = instanceIterator;
        }

        @Override
        public void onEpochEnd() throws IOException {
            super.model.evaluate(this.instanceIterator);
            this.instanceIterator.reset();
        }
    }

    public void train(InstanceIterator instanceIterator, int nEpoch, List<CallbackFunction> callbackFunctions) throws IOException {
        log.info("Training log-linear model.");
        for (CallbackFunction callbackFunction : callbackFunctions)
            callbackFunction.register(this);
        for (CallbackFunction callbackFunction : callbackFunctions)
            callbackFunction.onTrainBegin();
        for (int epoch = 0; epoch < nEpoch; ++epoch) {
            LogLinearXYData data = new LogLinearXYData(instanceIterator.getFeatureAlphabet());
            while (instanceIterator.hasNext()) {
                Instance inst = instanceIterator.next();
                FeatureVector[] fvs = inst.getFeatures();
                double[] weights = inst.getWeights();
                for (int y = 0; y < fvs.length; ++y) {
                    double weight = weights[y];
                    if (weight > 0.)
                        data.addEx(weight, IGNORED_X, y, fvs);
                }
            }
            for (CallbackFunction callbackFunction : callbackFunctions)
                callbackFunction.onEpochBegin();
            model = logLinearModel.train(data, model);
            instanceIterator.reset();
            for (CallbackFunction callbackFunction : callbackFunctions)
                callbackFunction.onEpochEnd();
        }
        instanceIterator.close();
        for (CallbackFunction callbackFunction : callbackFunctions)
            callbackFunction.onTrainEnds();
    }

    private double dotProd(IntDoubleVector params, FeatureVector fv) {
        double score = 0.;
        for (int i : fv.getInternalIndices().clone())
            score += params.get(i) * fv.get(i);
        return score;
    }

    private int predict(IntDoubleVector params, Instance instance) {
        return dotProd(params, instance.getFeatures()[0]) > 0 ? 0 : 1;
    }

    private int gold(Instance instance) {
        return instance.getWeights()[0] > 0 ? 0 : 1;
    }

    public void evaluate(InstanceIterator instanceIterator) {
        this.alphabet = instanceIterator.getFeatureAlphabet();
        IntDoubleVector params = this.model.getParams();
        int tt = 0, tt_correct = 0, tt_correct0 = 0, tt_gold0 = 0, tt_pred0 = 0;
        while (instanceIterator.hasNext()) {
            Instance inst = instanceIterator.next();
            int pred = predict(params, inst), gold = gold(inst);
            tt++;
            int correct = pred == gold ? 1 : 0;
            tt_correct += correct;
            if (pred == 0) {
                tt_correct0 += correct;
                tt_pred0++;
            }
            if (gold == 0)
                tt_gold0++;
        }
        double precision = (double) tt_correct0 / tt_pred0;
        double recall = (double) tt_correct0 / tt_gold0;
        double f1 = 2. / (1 / precision + 1 / recall);
        double acc = (double) tt_correct / tt;
        log.info(String.format("precision:%.2f%%,recall:%.2f%%,F1:%.2f%%,Acc:%.2f%%\n", precision * 100, recall * 100, f1 * 100, acc * 100));
    }

}

package model;

import edu.jhu.pacaya.gm.data.FgExampleList;
import edu.jhu.pacaya.gm.data.FgExampleMemoryStore;
import edu.jhu.pacaya.gm.data.LFgExample;
import edu.jhu.pacaya.gm.data.LabeledFgExample;
import edu.jhu.pacaya.gm.feat.FeatureVector;
import edu.jhu.pacaya.gm.feat.StringIterable;
import edu.jhu.pacaya.gm.inf.BeliefPropagation.BeliefPropagationPrm;
import edu.jhu.pacaya.gm.inf.BeliefPropagation.BpScheduleType;
import edu.jhu.pacaya.gm.inf.BeliefPropagation.BpUpdateOrder;
import edu.jhu.pacaya.gm.maxent.LogLinearXY.LogLinearXYPrm;
import edu.jhu.pacaya.gm.model.*;
import edu.jhu.pacaya.gm.model.Var.VarType;
import edu.jhu.pacaya.util.semiring.LogSemiring;
import edu.jhu.prim.bimap.IntObjectBimap;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dqwang on 12/8/16.
 */

public class LogLinearModel {

    private static final Logger log = Logger.getLogger(LogLinearModel.class);

    private LogLinearXYPrm prm;

    public LogLinearModel(LogLinearXYPrm prm) {
        this.prm = prm;
    }

    private IntObjectBimap<String> alphabet = null;

    private Map<Integer, List<String>> stateNamesMap;


    /**
     * Trains a log-linear model.
     *
     * @param data The log-linear model training examples created by
     *             LogLinearData.
     * @return Trained model.
     */
    public FgModel train(LogLinearXYData data, FgModel pretrained) {
        IntObjectBimap<String> alphabet = data.getFeatAlphabet();
       if (this.alphabet == null) {
            this.alphabet = alphabet;
            this.stateNamesMap = new HashMap<>();
        }
        FgExampleList list = getData(data);
        log.info("Number of train instances: " + list.size());
        log.info("Number of model parameters: " + alphabet.size());
        FgModel model = pretrained == null ? new FgModel(alphabet.size(), new StringIterable(alphabet.getObjects())) : pretrained;
        ModelTrainer trainer = new ModelTrainer(prm.crfPrm);
        trainer.train(model, list, null);
        return model;
    }

    /**
     * For testing only. Converts to the graphical model's representation of the data.
     */
    public FgExampleList getData(LogLinearXYData data) {
        List<LogLinearXYData.LogLinearExample> exList = data.getData();
        FgExampleMemoryStore store = new FgExampleMemoryStore();
        for (final LogLinearXYData.LogLinearExample desc : exList) {
            LFgExample ex = getFgExample(desc);
            store.add(ex);
        }
        return store;
    }

    private LFgExample getFgExample(final LogLinearXYData.LogLinearExample desc) {
        if (alphabet == null) {
            throw new IllegalStateException("decode can only be called after train");
        }
        Var v0 = getVar(desc.getNumYs());
        final VarConfig trainConfig = new VarConfig();
        trainConfig.put(v0, desc.getY());

        FactorGraph fg = new FactorGraph();
        VarSet vars = new VarSet(v0);
        ExpFamFactor f0 = new ExpFamFactor(vars) {

            @Override
            public FeatureVector getFeatures(int config) {
                return desc.getExample().getFeatures(config);
            }

        };
        fg.addFactor(f0);
        LabeledFgExample ex = new LabeledFgExample(fg, trainConfig);
        ex.setWeight(desc.getWeight());
        return ex;
    }

    private Var getVar(int numYs) {
        List<String> stateNames;
        if (stateNamesMap.containsKey(numYs)) {
            stateNames = stateNamesMap.get(numYs);
        } else {
            stateNames = new ArrayList<>();
            for (int y = 0; y < numYs; ++y)
                stateNames.add("" + y);
            stateNamesMap.put(numYs, stateNames);
        }
        return new Var(VarType.PREDICTED, stateNames.size(), "v0", stateNames);
    }

    private BeliefPropagationPrm getBpPrm() {
        BeliefPropagationPrm bpPrm = new BeliefPropagationPrm();
        bpPrm.s = LogSemiring.getInstance();
        bpPrm.schedule = BpScheduleType.TREE_LIKE;
        bpPrm.updateOrder = BpUpdateOrder.SEQUENTIAL;
        bpPrm.normalizeMessages = false;
        return bpPrm;
    }

}

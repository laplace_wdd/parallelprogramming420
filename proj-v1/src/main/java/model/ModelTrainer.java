package model;

/**
 * Created by dqwang on 12/8/16.
 */

import edu.jhu.hlt.optimize.SGD;
import edu.jhu.hlt.optimize.function.*;
import edu.jhu.pacaya.gm.data.FgExampleList;
import edu.jhu.pacaya.gm.model.FgModel;
import edu.jhu.pacaya.gm.train.CrfTrainer.CrfTrainerPrm;
import edu.jhu.pacaya.gm.train.CrfTrainer.Trainer;
import edu.jhu.pacaya.gm.train.EmpiricalRisk.EmpiricalRiskFactory;
import edu.jhu.pacaya.gm.train.LogLikelihoodFactory;
import edu.jhu.pacaya.gm.train.MtFactory;
import edu.jhu.pacaya.gm.train.ScaleByWeightFactory;
import model.AvgBatchObjective.ExampleObjective;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModelTrainer {

    private static final Logger log = LoggerFactory.getLogger(ModelTrainer.class);

    private CrfTrainerPrm prm;

    public ModelTrainer(CrfTrainerPrm prm) {
        this.prm = prm;
        if (prm.optimizer != null && prm.batchOptimizer != null) {
            throw new IllegalStateException("Only one of optimizer and batchOptimizer may be set in CrfTrainerPrm.");
        }
    }

    public FgModel train(FgModel model, FgExampleList data, Function validation) {
        ExampleObjective exObj;
        boolean isMinimize;
        MtFactory mtFactory;
        if (prm.trainer == Trainer.ERMA) {
            mtFactory = new EmpiricalRiskFactory(prm.bFactory, prm.dlFactory);
            isMinimize = true;
        } else {
            mtFactory = new LogLikelihoodFactory(prm.infFactory);
            isMinimize = false;
        }
        mtFactory = new ScaleByWeightFactory(mtFactory);
        exObj = new ModuleObjective(data, mtFactory);
        AvgBatchObjective objective = new AvgBatchObjective(exObj, model);

        Regularizer reg = prm.regularizer;
        if (prm.optimizer != null) {
            DifferentiableFunction fn = objective;
            if (reg != null) {
                reg.setNumDimensions(model.getNumParams());
                DifferentiableFunction nbr = isMinimize ? DifferentiableFunctionOpts.negate(reg) : reg;
                fn = new DifferentiableFunctionOpts.AddFunctions(objective, nbr);
            }
            if (isMinimize) {
                prm.optimizer.minimize(fn, model.getParams());
            } else {
                prm.optimizer.maximize(fn, model.getParams());
            }
            log.info("Final objective value: " + fn.getValue(model.getParams()));
        } else {
            DifferentiableBatchFunction fn = objective;
            if (reg != null) {
                // We don't need to rescale the regularizer because the CRF
                // objective is the average log-likelihood.
                reg.setNumDimensions(model.getNumParams());
                DifferentiableBatchFunction br = new FunctionAsBatchFunction(reg, objective.getNumExamples());
                DifferentiableBatchFunction nbr = isMinimize ? new BatchFunctionOpts.NegateFunction(br) : br;
                fn = new BatchFunctionOpts.AddFunctions(objective, nbr);
            }
            if (prm.batchOptimizer instanceof SGD && validation != null) {
                SGD sgd = (SGD) prm.batchOptimizer;
                sgd.optimize(fn, model.getParams(), !isMinimize, validation);
            } else {
                if (isMinimize) {
                    prm.batchOptimizer.minimize(fn, model.getParams());
                } else {
                    prm.batchOptimizer.maximize(fn, model.getParams());
                }
            }
        }
        return model;
    }

}

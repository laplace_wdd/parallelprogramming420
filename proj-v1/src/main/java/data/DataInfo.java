package data;

import edu.jhu.prim.bimap.IntObjectBimap;
import util.Constants;
import util.Functions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by dqwang on 12/8/16.
 */
public class DataInfo {

    private IntObjectBimap<String> featureAlphabet;
    private Set<String> vocab;

    private static void updateVocabFreqMap(String fileName, HashMap<String, Double> freqMap) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
        String line;
        while ((line = reader.readLine()) != null) {
            line = line.trim().split("\\t")[1];
            for (String x : line.trim().split(" "))
                Functions.update(freqMap, x, 1.);
        }
        reader.close();
    }

    private static void updateFeatureFreqMap(String fileName, HashMap<String, Double> freqMap, Set<String> vocab) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
        String line;
        while ((line = reader.readLine()) != null) {
            String x = Constants.BOS;
            line = line.trim().split("\\t")[1];
            for (String y : line.trim().split(" ")) {
                y = vocab.contains(y) ? y : Constants.OOV;
                Functions.update(freqMap, Functions.buildBiGram(x, y), 1.);
                x = y;
            }
            Functions.update(freqMap, Functions.buildBiGram(x, Constants.EOS), 1.);
        }
        reader.close();
    }

    private static IntObjectBimap<String> buildFeatureAlphabet(String fileName, Set<String> vocab) throws IOException {
        HashMap<String, Double> featureFreqMap = new HashMap<>();
        updateFeatureFreqMap(fileName, featureFreqMap, vocab);
        IntObjectBimap<String> alphabet = new IntObjectBimap<>();
        alphabet.lookupIndex(Constants.DEFAULT, true);
        for (String key : vocab)
            alphabet.lookupIndex(key, true);
        for (String key : featureFreqMap.keySet())
            if (featureFreqMap.get(key) > Constants.threshold)
                alphabet.lookupIndex(key, true);
        alphabet.stopGrowth();
        return alphabet;
    }

    public DataInfo(String fileName) throws IOException {
        HashMap<String, Double> tknFreqMap = new HashMap<>();
        updateVocabFreqMap(fileName, tknFreqMap);
        this.vocab = new HashSet<>(Arrays.asList(new String[]{Constants.BOS, Constants.EOS, Constants.OOV}));
        for (String key : tknFreqMap.keySet())
            if (tknFreqMap.get(key) > Constants.threshold)
                this.vocab.add(key);
        this.featureAlphabet = buildFeatureAlphabet(fileName, this.vocab);
    }

    public IntObjectBimap<String> getFeatureAlphabet() {
        return this.featureAlphabet;
    }

    public Set<String> getVocab() {
        return this.vocab;
    }
}

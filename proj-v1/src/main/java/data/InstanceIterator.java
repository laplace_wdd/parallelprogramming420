package data;

import edu.jhu.pacaya.gm.feat.FeatureVector;
import edu.jhu.prim.bimap.IntObjectBimap;
import util.Constants;
import util.Functions;
import util.Instance;

import java.io.*;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by dqwang on 12/8/16.
 */
public class InstanceIterator implements Iterator<Instance> {
    private Instance current;
    private String fileName;
    private BufferedReader reader;
    private Set<String> vocab;
    private IntObjectBimap<String> featureAlphabet;

    public InstanceIterator(String fileName, Set<String> vocab, IntObjectBimap<String> alphabet) throws FileNotFoundException {
        this.fileName = fileName;
        this.reader = new BufferedReader(new FileReader(new File(fileName)));
        this.vocab = vocab;
        this.featureAlphabet = alphabet;
    }

    @Override
    public boolean hasNext() {
        String line = null;
        try {
            line = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.current = line != null ? line2Instance(line) : null;
        return this.current != null;
    }

    @Override
    public Instance next() {
        return this.current;
    }

    private Instance line2Instance(String line) {
        String[] items = line.trim().split("\\t");
        int y_label = Integer.parseInt(items[0]);
        double[] weights = {0., 0.};
        weights[y_label] += 1.;
        FeatureVector[] fvs = {new FeatureVector(), new FeatureVector()};
        String x = Constants.BOS;
        for (String y : items[1].split(" ")) {
            y = this.vocab.contains(y) ? y : Constants.OOV;
            Functions.addFeat(fvs[0], this.featureAlphabet.lookupIndex(y), 1.);
            String key = Functions.buildBiGram(x, y);
            Functions.addFeat(fvs[0], this.featureAlphabet.lookupIndex(key), 1.);
            x = y;
        }
        Functions.addFeat(fvs[0], this.featureAlphabet.lookupIndex(Functions.buildBiGram(x, Constants.EOS)), 1.);
        // Make the vector dense
        if (Constants.dense == 1)
            for (int i = 0; i < featureAlphabet.size(); ++i)
                Functions.addFeat(fvs[0], i, 0.001);
        return new Instance(fvs, weights);
    }

    @Override
    public void remove() {
        System.err.println("No such method");
    }

    public void close() throws IOException {
        this.reader.close();
    }

    public void reset() throws IOException {
        this.close();
        this.reader = new BufferedReader(new FileReader(new File(this.fileName)));
    }

    public IntObjectBimap<String> getFeatureAlphabet() {
        return this.featureAlphabet;
    }
}

import data.DataInfo;
import data.InstanceIterator;
import edu.jhu.pacaya.gm.maxent.LogLinearXY.LogLinearXYPrm;
import edu.jhu.pacaya.util.Threads;
import edu.jhu.pacaya.util.cli.ArgParser;
import edu.jhu.pacaya.util.cli.Opt;
import edu.jhu.pacaya.util.report.ReporterManager;
import edu.jhu.prim.util.random.Prng;
import model.Model;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import util.Constants;
import util.OptWrapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by dqwang on 12/8/16.
 */
public class Driver {
    private static final Logger log = Logger.getLogger(Driver.class);
    @Opt(name = "seed", description = "Pseudo random number generator seed for everything else.")
    public static long seed = 0;
    @Opt(description = "The location of the test data to be used if trainOnly is true.")
    public static String trainPath = "../data/gen_spam/train/gen_spam-times1";
    @Opt(description = "The location of the test data to be used if trainOnly is true.")
    public static String devPath = "../data/gen_spam/dev/gen_spam";
    @Opt(description = "The location of the test data to be used if trainOnly is true.")
    public static int nEpoch = 1;

    public static void train() throws IOException, ParseException {

        // build dictionary
        DataInfo trainInfo = new DataInfo(trainPath);
        // build model
        LogLinearXYPrm prm = new LogLinearXYPrm();
        prm.crfPrm = OptWrapper.getCrfTrainerPrm();
        Model model = new Model(prm);
        // load training data
        InstanceIterator trainInstanceIterator = new InstanceIterator(trainPath, trainInfo.getVocab(), trainInfo.getFeatureAlphabet());
        InstanceIterator devInstanceIterator = new InstanceIterator(devPath, trainInfo.getVocab(), trainInfo.getFeatureAlphabet());
        model.train(trainInstanceIterator,
                nEpoch,
                new LinkedList<Model.CallbackFunction>(
                        //         Arrays.asList(new Model.EvalDataFunction(devInstanceIterator))
                )
        );
        trainInstanceIterator.close();
        devInstanceIterator.close();
    }

    /**
     * Main entry point.
     */
    public static void main(String[] args) {
        log.info("Running with args: " + StringUtils.join(args, " "));
        int exitCode = 0;
        ArgParser parser = null;
        try {
            parser = new ArgParser(Driver.class);
            parser.registerClass(Driver.class);
            parser.registerClass(OptWrapper.class);
            parser.registerClass(Constants.class);
            parser.parseArgs(args);
            ReporterManager.init(ReporterManager.reportOut, true);
            Prng.seed(seed);
            Threads.initDefaultPool(OptWrapper.threads);
            Driver driver = new Driver();
            driver.train();
        } catch (ParseException e1) {
            log.error(e1.getMessage());
            if (parser != null)
                parser.printUsage();
            exitCode = 1;
        } catch (Throwable t) {
            t.printStackTrace();
            exitCode = 1;
        } finally {
            Threads.shutdownDefaultPool();
            ReporterManager.close();
        }
        System.exit(exitCode);
    }

}

package util;

import edu.jhu.pacaya.gm.feat.FeatureVector;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Created by dqwang on 12/8/16.
 */
public class Functions {
    public static String buildBiGram(String t1, String t2) {
        return StringUtils.join(new String[]{t1, t2}, "_");
    }
    public static String buildTriGram(String t1, String t2) {
        return StringUtils.join(new String[]{t1, t2}, "_");
    }

    public static <T> void update(Map<T, Double> map, T key, double val) {
        map.put(key, map.containsKey(key) ? map.get(key) + val : val);
    }

    public static void addFeat(FeatureVector fv, int fid, double value) {
        // Don't add features not in the alphabet.
        if (fid != -1) {
            assert !Double.isNaN(value) : "value=" + value;
            assert !Double.isInfinite(value) : "value=" + value;
            fv.add(fid, value);
        }
    }

}

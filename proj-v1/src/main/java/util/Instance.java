package util;

import edu.jhu.pacaya.gm.feat.FeatureVector;
import edu.jhu.prim.bimap.IntObjectBimap;

/**
 * Created by dqwang on 12/8/16.
 */
public class Instance {

    private FeatureVector[] features;

    private double[] weights;

    public Instance(FeatureVector[] features, double[] weights) {
        this.features = features;
        this.weights = weights;
    }

    public double[] getWeights() {
        return weights;
    }

    public FeatureVector[] getFeatures() {
        return features;
    }

    public void print(IntObjectBimap<String> featureAlphabet) {
        for (int i = 0; i < features.length; ++i) {
            System.out.print(i + "\t" + this.weights[i] + "\t");
            int[] indices = features[i].getInternalIndices().clone();
            for (int feat : indices) {
                System.out.print(feat);
                System.out.print("-");
                System.out.print(featureAlphabet.lookupObject(feat));
                System.out.print("-");
                System.out.print(features[i].get(feat));
                System.out.print("-");
                System.out.print(features[i].get(feat));
                System.out.print("-");
                System.out.print(features[i].get(feat));
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}

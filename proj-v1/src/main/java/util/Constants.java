package util;

import edu.jhu.pacaya.util.cli.Opt;

/**
 * Created by dqwang on 12/8/16.
 */
public class Constants {
    public static final String BOS = "<BOS>";   // special word type for context at Beginning Of Sequence
    public static final String EOS = "<EOS>";   // special word type for observed token at End Of Sequence
    public static final String OOV = "<OOV>";   // special word type for all Out-Of-Vocabulary words
    public static final String DEFAULT = "<DEFAULT>";   // special word type for all Out-Of-Vocabulary words
    @Opt(description = "Training policy ")
    public static String policy = "sync";   // special word type for all Out-Of-Vocabulary words
    @Opt(description = "OOV threshold")
    public static int threshold = 0;   // special word type for all Out-Of-Vocabulary words
    @Opt(description = "Whether using dense vector")
    public static int dense = 0;   // special word type for all Out-Of-Vocabulary words
}

#!/usr/bin/python
# --------------------------------------- 
# File Name : preproc.py
# Creation Date : 08-12-2016
# Last Modified : Sat Dec 10 12:35:12 2016
# Created By : wdd 
# --------------------------------------- 
import argparse
import random

_arg_parser = argparse.ArgumentParser()
_arg_parser.add_argument('--task', default='main', type=str, action='store', help='')
_arg_parser.add_argument('--x1', default='', type=str, action='store', help='')
_arg_parser.add_argument('--x2', default='', type=str, action='store', help='')
_args = _arg_parser.parse_args()


def _config(args_dict=vars(_args)):
    return '\n'.join("{0}:{1}".format(key, val) for (key, val) in args_dict.items())


def main():
    random.seed(0)
    data = ['0\t' + ' '.join(l.strip().split()) for l in open(_args.x1)]
    data += ['1\t' + ' '.join(l.strip().split()) for l in open(_args.x2)]
    random.shuffle(data)
    print '\n'.join(data)


if __name__ == "__main__": locals()[_args.task]()
